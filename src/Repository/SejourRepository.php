<?php

namespace App\Repository;

use App\Entity\Sejour;
use App\Form\Data\HomeSearchData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Stopwatch\Stopwatch;

class SejourRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Sejour::class);
    }

    public function homeSearch(HomeSearchData $searchData)
    {
        $queryBuilder = $this->createQueryBuilder('sejour');

        if (\is_null($searchData->getStartDate()) === false) {
            $queryBuilder
                ->andWhere('sejour.endDate > :dateStart')
                ->setParameter('dateStart', $searchData->getStartDate());
        }

        if (\is_null($searchData->getEndDate()) === false) {
            $queryBuilder
                ->andWhere('sejour.startDate < :endDate')
                ->setParameter('endDate', $searchData->getEndDate());
        }

        if (empty($searchData->getStatus()) === false) {
            $queryBuilder
                ->andWhere('sejour.status = :status')
                ->setParameter('status', $searchData->getStatus());
        }

        if (\is_null($searchData->getName()) === false) {
            $queryBuilder
                ->andWhere('sejour.customerLastname = :name')
                ->setParameter('name', $searchData->getName());
        }

        if (\is_null($searchData->getLocation()) === false) {
            $queryBuilder
                ->andWhere('sejour.emplacement = :location')
                ->setParameter('location', $searchData->getLocation());
        }

//        $query = 'SELECT * FROM sejour WHERE';
//
//        if (\is_null($searchData->getStartDate()) === false) {
//            $query = ' sejour.end_date > 2020-12-01';
//        }
//
//        if (\is_null($searchData->getStartDate()) === false) {
//            $query = ' AND sejour.status = "RESERVED"';
//        }


        return $queryBuilder
            ->getQuery()
            ->getResult();
    }

//    public function findNameById(int $id)
//    {
        // Tableau
//        $result = $this
//            ->createQueryBuilder('sejour')
//            ->select('sejour.id')
//            ->where('sejour.id = :id')
//            ->setParameter('id' , $id)
//            ->getQuery()
//            ->getArrayResult();


//        $stopWatch = new Stopwatch();
//
//        $stopWatch->start('aa');
//        // Tableau d'objet
//        $result = $this
//            ->createQueryBuilder('sejour')
//            ->where('sejour.id = :id')
//            ->setParameter('id' , $id)
//            ->getQuery()
//            ->getResult();
//
//        $stopWatch->stop('aa');
//        dump($stopWatch->getEvent('aa')->getDuration() . ' ms');

        // Pur MYSQL
//        $stopWatch->start('cc');
//        $connection = $this->getEntityManager()->getConnection();
//        $result = $connection->executeQuery(
//            'SELECT id FROM sejour WHERE id = ?',
//            [$id],
//            [\PDO::PARAM_INT]
//        )->fetchAll();

//        $stopWatch->stop('cc');
//        dump($stopWatch->getEvent('cc')->getDuration() . ' ms');

//        dump($result);
//        die;
//    }
}
